package com.example.themes;

import android.os.Bundle;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		new ThemeColors(this);
		setContentView(R.layout.activity_main);
		findViewById(R.id.change_color).setOnClickListener(v -> changeColor());
	}

	private void changeColor() {

		String accent = ((EditText) findViewById(R.id.text_accent)).getText().toString();
		String primary = ((EditText) findViewById(R.id.text_primary)).getText().toString();
		String dark = ((EditText) findViewById(R.id.text_dark)).getText().toString();

		if (accent.length() != 6) {
			accent = null;
		}
		if (primary.length() != 6) {
			primary = null;
		}
		if (dark.length() != 6) {
			dark = null;
		}

		ThemeColors.setNewThemeColor(this, accent, primary, dark);

//		int red= new Random().nextInt(255);
//		int green= new Random().nextInt(255);
//		int blue= new Random().nextInt(255);
//		ThemeColors.setNewThemeColor(MainActivity.this, red, green, blue);

//		new Thread(() -> {
//			File root = new File("/sdcard");
//			File gpxfile = new File(root, "styles_colors.xml");
//			FileWriter writer;
//			try {
//				Log.e("KANVAS", "CREATING COLORS");
//				writer = new FileWriter(gpxfile);
//				for(int red = 0; red <= 255; red += 15) {
//					for(int green = 0; green <= 255; green += 15) {
//						for(int blue = 0; blue <= 255; blue += 15) {
//							String color = Integer.toHexString(Color.rgb(red, green, blue)).substring(2);
//							writer.append("<style name=\"A_" + color + "\">");
//							writer.append("<item name=\"colorAccent\">#" + color + "</item></style>");
//							writer.append("<style name=\"P_" + color + "\">");
//							writer.append("<item name=\"colorPrimary\">#" + color + "</item></style>");
//							writer.append("<style name=\"D_" + color + "\">");
//							writer.append("<item name=\"colorPrimaryDark\">#" + color + "</item></style>");
//						}
//					}
//				}
//				writer.flush();
//				writer.close();
//				Log.e("KANVAS", "CREATING COLORS DONE");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}).start();

	}
}
