package com.example.themes;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import androidx.annotation.ColorInt;

public class ThemeColors {

	private static final String NAME = "ThemeColors";
	private static final String KEY_ACCENT = "accent";
	private static final String KEY_PRIMARY = "primary";
	private static final String KEY_PRIMARY_DARK = "primaryDark";

	@ColorInt
	public int color;

	public ThemeColors(Context context) {
		SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
		String accent = sharedPreferences.getString(KEY_ACCENT, null);
		String primary = sharedPreferences.getString(KEY_PRIMARY, null);
		String dark = sharedPreferences.getString(KEY_PRIMARY_DARK, null);

		Log.e("KANVAS", "READ " + accent + "/" + primary + "/" + dark);
		if (accent != null) {
			context.getTheme().applyStyle(context.getResources().getIdentifier("A_" + accent, "style",
					context.getPackageName()), true);
		}
		if (primary != null) {
			context.getTheme().applyStyle(context.getResources().getIdentifier("P_" + primary, "style",
					context.getPackageName()), true);
		}
		if (dark != null) {
			context.getTheme().applyStyle(context.getResources().getIdentifier("D_" + dark, "style",
					context.getPackageName()), true);
		}

	}

	public static void setNewThemeColor(Activity activity, String accent, String primary, String dark) {
		if (accent != null) {
			setColorStyle(activity, accent, KEY_ACCENT);
		}
		if (primary != null) {
			setColorStyle(activity, primary, KEY_PRIMARY);
		}
		if (dark != null) {
			setColorStyle(activity, dark, KEY_PRIMARY_DARK);
		}

		activity.recreate();
	}

	private static void setColorStyle(Context context, String color, String key) {
		int red = getComponent(Integer.valueOf(color.substring(0, 2), 16));
		int green = getComponent(Integer.valueOf(color.substring(2, 4), 16));
		int blue = getComponent(Integer.valueOf(color.substring(4, 6), 16));
		String stringColor = Integer.toHexString(Color.rgb(red, green, blue)).substring(2);

		Log.e("KANVAS", "SAVE " + key + ":" + stringColor);
		SharedPreferences.Editor editor = context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit();
		editor.putString(key, stringColor);
		editor.apply();
	}

	private static int getComponent(int component) {
		double floor = (15 * (Math.floor(component / 15.f)));
		double ceil = (15 * (Math.ceil(component / 15.f)));
		if (Math.abs(component - floor) < Math.abs(component - ceil)) {
			return (int) floor;
		}
		return (int) ceil;
	}

}